package ru.tsc.karbainova.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import ru.tsc.karbainova.tm.api.service.ICommandService;

public abstract class AbstractSystemCommand extends AbstractCommand{
    @NotNull
    @Autowired
    protected ICommandService commandService;

    @Nullable
    public abstract String arg();

    @Override
    @NotNull
    public String toString() {
        @NotNull String result = super.toString();
        @Nullable final String arg = arg();
        if (arg != null && !arg.isEmpty()) result += " (" + arg + ")";
        return result;
    }

}
