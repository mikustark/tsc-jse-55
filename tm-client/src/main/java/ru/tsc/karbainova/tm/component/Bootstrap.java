package ru.tsc.karbainova.tm.component;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.karbainova.tm.api.service.ICommandService;
import ru.tsc.karbainova.tm.api.service.ILogService;
import ru.tsc.karbainova.tm.api.service.IPropertyService;
import ru.tsc.karbainova.tm.command.AbstractCommand;
import ru.tsc.karbainova.tm.endpoint.*;
import ru.tsc.karbainova.tm.exception.system.UnknowCommandException;

import java.util.Arrays;
import java.util.Scanner;

@Getter
@Setter
@Component
public class Bootstrap {

    @Autowired
    private ILogService logService;
    @Autowired
    private IPropertyService propertyService;
    @Autowired
    private ICommandService commandService;

    @Nullable
    private static SessionDTO session;
    @Nullable
    @Autowired
    private AbstractCommand[] commands;


    public void start(@NonNull final String... args) {
        System.out.println("** WELCOME TO TASK MANAGER **");
        initCommand();
        parseArgs(args);
        startInput();
    }

    public void parseArgs(@Nullable final String[] args) {
        if (args == null || args.length == 0) return;
        @Nullable final String arg = args[0];
        parseArg(arg);
        System.exit(0);
    }
    public void parseArg(@Nullable final String arg) {
        if (arg == null || arg.isEmpty()) return;
        @Nullable final AbstractCommand command = commandService.getCommandByArg(arg);
        if (command == null) return;
        command.execute();
    }

    public void startInput() {
        logService.debug("process start...");
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while (!"exit".equals(command)) {
            try {
                System.out.println("Enter command:");
                command = scanner.nextLine();
                executeCommand(command);
                logService.info("`" + command + "` command executed");
            } catch (Exception e) {
                logService.error(e);
                System.out.println();
            }
        }
    }

    @SneakyThrows
    public void initCommand() {
        Arrays.stream(commands).forEach(command -> commandService.add(command));
    }

    public void executeCommand(@NonNull final String commandName) {
        if (commandName.isEmpty() || commandName == null) return;
        AbstractCommand abstractCommand = commandService.getCommandByName(commandName);
        if (abstractCommand == null) throw new UnknowCommandException(commandName);
        abstractCommand.execute();
    }
}
