package ru.tsc.karbainova.tm.command.auth;

import org.springframework.stereotype.Component;
import ru.tsc.karbainova.tm.command.AbstractCommand;
import ru.tsc.karbainova.tm.command.AbstractSystemCommand;
import ru.tsc.karbainova.tm.command.TerminalUtil;
import ru.tsc.karbainova.tm.endpoint.SessionDTO;

@Component
public class AuthLoginCommand extends AbstractSystemCommand {
    @Override
    public String name() {
        return "login";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Login app";
    }

    @Override
    public void execute() {
        System.out.println("Enter login:");
        final String login = TerminalUtil.nextLine();
        System.out.println("Enter password:");
        final String password = TerminalUtil.nextLine();
        SessionDTO session = sessionEndpoint.openSession(login, password);
        sessionService.setSession(session);

        System.out.println("[OK]");
    }
}
