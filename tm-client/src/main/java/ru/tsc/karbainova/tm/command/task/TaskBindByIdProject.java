package ru.tsc.karbainova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.karbainova.tm.command.AbstractSystemCommand;
import ru.tsc.karbainova.tm.command.TerminalUtil;
import ru.tsc.karbainova.tm.endpoint.SessionDTO;

@Component
public class TaskBindByIdProject extends AbstractSystemCommand {
    @Override
    @NotNull
    public String name() {
        return "task-bind-by-id-project";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return "Bind task to project by id.";
    }

    @Override
    public void execute() {
        System.out.print("Enter project id: ");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.print("Enter task id: ");
        @NotNull final String taskId = TerminalUtil.nextLine();
        projectEndpoint.taskBindByIdProject(sessionService.getSession(), projectId, taskId);
    }

}
