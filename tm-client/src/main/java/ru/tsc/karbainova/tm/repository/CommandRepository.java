package ru.tsc.karbainova.tm.repository;

import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.karbainova.tm.api.repository.ICommandRepository;
import ru.tsc.karbainova.tm.command.AbstractCommand;
import ru.tsc.karbainova.tm.command.AbstractSystemCommand;

import java.util.*;

@Repository
public class CommandRepository implements ICommandRepository {

    @Nullable
    private final Map<String, AbstractSystemCommand> arguments = new LinkedHashMap<>();
    @NonNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @Override
    public AbstractCommand getCommandByName(@NonNull String name) {
        return commands.get(name);
    }

    @Override
    public AbstractCommand getCommandByArg(@Nullable String arg) {
        return commands.get(arg);
    }

    @NonNull
    @Override
    public Map<String, AbstractCommand> getCommands() {
        return commands;
    }

    @Nullable
    @Override
    public Map<String, AbstractSystemCommand> getArguments() {
        return arguments;
    }

    @Override
    public Collection<String> getCommandArg() {
        final List<String> result = new ArrayList<>();
        for (final AbstractSystemCommand command : arguments.values()) {
            final String arg = command.arg();
            if (arg == null || arg.isEmpty()) continue;
            result.add(arg);
        }
        return result;
    }

    @Override
    public Collection<String> getCommandName() {
        final List<String> result = new ArrayList<>();
        for (final AbstractCommand command : commands.values()) {
            final String name = command.name();
            if (name == null || name.isEmpty()) continue;
            result.add(name);
        }
        return result;
    }

    @Override
    public void create(AbstractSystemCommand command) {
        final String name = command.name();
        final String arg = command.arg();
        if (arg != null) arguments.put(arg, command);
        if (name != null) commands.put(name, command);
    }

    @Override
    public void add(@NotNull AbstractCommand command) {
        if (command instanceof AbstractSystemCommand) {
            @NotNull final AbstractSystemCommand systemCommand;
            systemCommand = (AbstractSystemCommand) command;
            @Nullable final String arg = systemCommand.arg();
            if (arg != null) arguments.put(arg, systemCommand);
        }
        @Nullable final String name = command.name();
        if (name != null) commands.put(name, command);
    }
}
