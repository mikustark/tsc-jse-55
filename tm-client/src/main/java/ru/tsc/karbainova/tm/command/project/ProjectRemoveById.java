package ru.tsc.karbainova.tm.command.project;

import org.springframework.stereotype.Component;
import ru.tsc.karbainova.tm.command.AbstractSystemCommand;
import ru.tsc.karbainova.tm.command.TerminalUtil;
import ru.tsc.karbainova.tm.endpoint.SessionDTO;

@Component
public class ProjectRemoveById extends AbstractSystemCommand {
    @Override
    public String name() {
        return "remove-by-id-project";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove by index";
    }

    @Override
    public void execute() {

        System.out.println("Enter project id: ");
        final String projectId = TerminalUtil.nextLine();
        projectEndpoint.removeProjectById(sessionService.getSession(), projectId);
    }

}
