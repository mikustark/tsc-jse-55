package ru.tsc.karbainova.tm.command.serv;

import org.springframework.stereotype.Component;
import ru.tsc.karbainova.tm.command.AbstractSystemCommand;

@Component
public class ExitCommand extends AbstractSystemCommand {
    @Override
    public String name() {
        return "exit";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Exit app";
    }

    @Override
    public void execute() {
        System.exit(0);
    }
}
