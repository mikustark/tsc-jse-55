package ru.tsc.karbainova.tm.command.serv;

import com.jcabi.manifests.Manifests;
import org.springframework.stereotype.Component;
import ru.tsc.karbainova.tm.command.AbstractSystemCommand;

@Component
public class VersionDisplayCommand extends AbstractSystemCommand {
    @Override
    public String name() {
        return "version";
    }

    @Override
    public String arg() {
        return "-v";
    }

    @Override
    public String description() {
        return "Version";
    }

    @Override
    public void execute() {
        System.out.println(propertyService.getApplicationVersion());
        System.out.println(Manifests.read("build"));
    }
}
