package ru.tsc.karbainova.tm.service;

import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.karbainova.tm.api.repository.ICommandRepository;
import ru.tsc.karbainova.tm.api.service.ICommandService;
import ru.tsc.karbainova.tm.command.AbstractCommand;
import ru.tsc.karbainova.tm.command.AbstractSystemCommand;

import java.util.Collection;
import java.util.Map;

@Service
public class CommandService implements ICommandService {

    @Autowired
    private ICommandRepository commandRepository;

    @Override
    public AbstractCommand getCommandByName(@NonNull final String name) {
        if (name.isEmpty()) return null;
        return commandRepository.getCommandByName(name);
    }

    @Override
    public AbstractCommand getCommandByArg(@Nullable final String arg) {
        if (arg == null || arg.isEmpty()) return null;
        return commandRepository.getCommandByArg(arg);
    }

    @Override
    public Map<String, AbstractCommand> getCommands() {
        return commandRepository.getCommands();
    }

    @Override
    public Map<String, AbstractSystemCommand> getArguments() {
        return commandRepository.getArguments();
    }

    @Override
    public Collection<String> getListCommandName() {
        return commandRepository.getCommandName();
    }

    @Override
    public Collection<String> getListCommandArg() {
        return commandRepository.getCommandArg();
    }

    @Override
    public void create(AbstractSystemCommand command) {
        if (command == null) return;
        commandRepository.create(command);
    }

    @Override
    public void add(@NotNull final AbstractCommand command) {
        commandRepository.add(command);
    }

}
