package ru.tsc.karbainova.tm.command.user;

import ru.tsc.karbainova.tm.command.AbstractSystemCommand;
import ru.tsc.karbainova.tm.command.TerminalUtil;
import ru.tsc.karbainova.tm.endpoint.SessionDTO;

public class UserUnlockByLoginCommand extends AbstractSystemCommand {
    @Override
    public String name() {
        return "user-unlock-by-login";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "User unlock";
    }

    @Override
    public void execute() {
        System.out.println("Enter login");
        final String login = TerminalUtil.nextLine();
        adminEndpoint.unlockUserByLoginUser(sessionService.getSession(), login);
    }

}
