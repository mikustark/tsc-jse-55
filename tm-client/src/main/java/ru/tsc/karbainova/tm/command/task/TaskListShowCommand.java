package ru.tsc.karbainova.tm.command.task;

import org.springframework.stereotype.Component;
import ru.tsc.karbainova.tm.command.AbstractSystemCommand;
import ru.tsc.karbainova.tm.endpoint.SessionDTO;
@Component
public class TaskListShowCommand extends AbstractSystemCommand {
    @Override
    public String name() {
        return "task-list";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show all tascks";
    }

    @Override
    public void execute() {
        taskEndpoint.findAllTask(sessionService.getSession()).stream().forEach(o -> System.out.println(o.getName()));
        System.out.println("[OK]");
    }
}
