package ru.tsc.karbainova.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.command.AbstractCommand;
import ru.tsc.karbainova.tm.command.AbstractSystemCommand;

import java.util.Collection;
import java.util.Map;

public interface ICommandService {

    AbstractCommand getCommandByName(String name);

    AbstractCommand getCommandByArg(String arg);

    Map<String, AbstractCommand> getCommands();

    Map<String, AbstractSystemCommand> getArguments();

    Collection<String> getListCommandName();

    Collection<String> getListCommandArg();

    void create(AbstractSystemCommand command);

    void add(@Nullable AbstractCommand command);

}
