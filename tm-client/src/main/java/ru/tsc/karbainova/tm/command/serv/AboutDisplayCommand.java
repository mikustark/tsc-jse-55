package ru.tsc.karbainova.tm.command.serv;

import com.jcabi.manifests.Manifests;
import org.springframework.stereotype.Component;
import ru.tsc.karbainova.tm.command.AbstractSystemCommand;

@Component
public class AboutDisplayCommand extends AbstractSystemCommand {
    @Override
    public String name() {
        return "about";
    }

    @Override
    public String arg() {
        return "-a";
    }

    @Override
    public String description() {
        return "About dev";
    }

    @Override
    public void execute() {
        System.out.println(propertyService.getDeveloperName());
        System.out.println(Manifests.read("developer"));
        System.out.println(propertyService.getDeveloperEmail());
    }
}
