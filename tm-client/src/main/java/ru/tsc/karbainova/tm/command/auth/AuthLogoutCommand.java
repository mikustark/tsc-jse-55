package ru.tsc.karbainova.tm.command.auth;

import ru.tsc.karbainova.tm.command.AbstractCommand;
import ru.tsc.karbainova.tm.command.AbstractSystemCommand;
import ru.tsc.karbainova.tm.endpoint.SessionDTO;

public class AuthLogoutCommand extends AbstractSystemCommand {
    @Override
    public String name() {
        return "logout";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Logout app";
    }

    @Override
    public void execute() {
        System.out.println("Logout app");
        SessionDTO session = sessionService.getSession();

        sessionEndpoint.closeSession(session);
    }
}
