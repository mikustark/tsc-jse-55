package ru.tsc.karbainova.tm.command.project;

import org.springframework.stereotype.Component;
import ru.tsc.karbainova.tm.command.AbstractSystemCommand;
import ru.tsc.karbainova.tm.endpoint.SessionDTO;

@Component
public class ProjectListShowCommand extends AbstractSystemCommand {
    @Override
    public String name() {
        return "project-list";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show all projects";
    }

    @Override
    public void execute() {
        System.out.println("Project list");
        projectEndpoint.findAllProject(sessionService.getSession()).stream().forEach(o -> System.out.println(o.getId() + " " + o.getName()));
        System.out.println("[OK]");
    }
}
