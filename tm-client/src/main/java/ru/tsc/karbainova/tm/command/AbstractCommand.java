package ru.tsc.karbainova.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import ru.tsc.karbainova.tm.api.service.IPropertyService;
import ru.tsc.karbainova.tm.endpoint.*;
import ru.tsc.karbainova.tm.service.SessionService;


public abstract class AbstractCommand {
    @NotNull
    @Autowired
    protected IPropertyService propertyService;

    @NotNull
    @Autowired
    protected AdminUserEndpoint adminEndpoint;

    @NotNull
    @Autowired
    protected UserEndpoint userEndpoint;

    @NotNull
    @Autowired
    protected ProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    protected TaskEndpoint taskEndpoint;

    @NotNull
    @Autowired
    protected SessionEndpoint sessionEndpoint;

    @NotNull
    @Autowired
    protected SessionService sessionService;

    @Nullable
    public abstract String name();

    @Nullable
    public abstract String description();

    public abstract void execute();

    @Nullable
    public Role[] roles(){
        return null;
    }

    @Override
    @NotNull
    public String toString() {
        @NotNull String result = "";
        @Nullable final String name = name();
        @Nullable final String description = description();
        if(name != null && !name.isEmpty()) result += name + " ";
        if(description != null && !description.isEmpty()) result += ": " + description;
        return result;
    }


}
