package ru.tsc.karbainova.tm.command.serv;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.karbainova.tm.command.AbstractCommand;
import ru.tsc.karbainova.tm.command.AbstractSystemCommand;

import java.util.Collection;

@Component
public class HelpDisplayCommand extends AbstractSystemCommand {
    @Override
    public String name() {
        return "help";
    }

    @Override
    public String arg() {
        return "-h";
    }

    @Override
    public String description() {
        return "All commands";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        @NotNull final Collection<AbstractCommand> commands = (Collection<AbstractCommand>) commandService.getCommands();
        for (AbstractCommand command : commands) {
            System.out.println(command);
        }
    }
}
