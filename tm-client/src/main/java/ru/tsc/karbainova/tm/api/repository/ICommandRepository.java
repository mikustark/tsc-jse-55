package ru.tsc.karbainova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.karbainova.tm.command.AbstractCommand;
import ru.tsc.karbainova.tm.command.AbstractSystemCommand;

import java.util.Collection;
import java.util.Map;

public interface ICommandRepository {

    AbstractCommand getCommandByName(String name);

    AbstractCommand getCommandByArg(String arg);

    Map<String, AbstractCommand> getCommands();

    Map<String, AbstractSystemCommand> getArguments();

    Collection<String> getCommandArg();

    Collection<String> getCommandName();

    void create(AbstractSystemCommand command);

    void add(@NotNull AbstractCommand command);
}
