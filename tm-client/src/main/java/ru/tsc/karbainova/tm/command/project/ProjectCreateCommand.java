package ru.tsc.karbainova.tm.command.project;

import org.springframework.stereotype.Component;
import ru.tsc.karbainova.tm.command.AbstractSystemCommand;
import ru.tsc.karbainova.tm.command.TerminalUtil;
import ru.tsc.karbainova.tm.endpoint.SessionDTO;

@Component
public class ProjectCreateCommand extends AbstractSystemCommand {
    @Override
    public String name() {
        return "create-project";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Create project";
    }

    @Override
    public void execute() {

        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        final String description = TerminalUtil.nextLine();
        projectEndpoint.createProjectAllParam(sessionService.getSession(), name, description);
        System.out.println("[OK]");
    }
}
