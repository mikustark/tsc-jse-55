package ru.tsc.karbainova.tm.command.task;

import org.springframework.stereotype.Component;
import ru.tsc.karbainova.tm.command.AbstractSystemCommand;
import ru.tsc.karbainova.tm.command.TerminalUtil;
import ru.tsc.karbainova.tm.endpoint.SessionDTO;
@Component
public class TaskCreateCommand extends AbstractSystemCommand {
    @Override
    public String name() {
        return "create-task";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Create task";
    }

    @Override
    public void execute() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        final String description = TerminalUtil.nextLine();
        taskEndpoint.createTaskAllParam(sessionService.getSession(), name, description);
        System.out.println("[OK]");
    }

}
