package ru.tsc.karbainova.tm.exception.empty;

import ru.tsc.karbainova.tm.exception.AbstractException;

public class EmptyPasswordException extends AbstractException {
    public EmptyPasswordException() {
        super("Error Password.");
    }

    public EmptyPasswordException(String value) {
        super("Error Password. " + value);
    }
}
