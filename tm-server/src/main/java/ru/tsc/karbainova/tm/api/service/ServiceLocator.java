package ru.tsc.karbainova.tm.api.service;

import ru.tsc.karbainova.tm.api.service.dto.*;

public interface ServiceLocator {
    ITaskService getTaskService();

    IProjectService getProjectService();

    IUserService getUserService();

    ISessionService getSessionService();

    IAdminUserService getAdminUserService();

    IProjectToTaskService getProjectToTaskService();
}
